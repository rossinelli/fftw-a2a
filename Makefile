CFLAGS += -std=c99
CFLAGS += -pedantic
CFLAGS += -D_POSIX_C_SOURCE=200112L
CFLAGS += -fno-finite-math-only

LDFLAGS += -lm -lmpi

GOALS = fftw-a2a.fp32 fftw-a2a.fp64

all : $(GOALS)

fftw-a2a.fp32 : main.c
	$(CC) $(CFLAGS) -D_FP32_ -o $@ $< -lfftw3f_mpi -lfftw3f $(LDFLAGS)

fftw-a2a.fp64 : main.c
	$(CC) $(CFLAGS) -o $@ $< -lfftw3_mpi -lfftw3 $(LDFLAGS)

clean :
	rm -f $(GOALS) fftw-a2a

.PHONY : clean
