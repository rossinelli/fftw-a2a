#include <stdio.h>
#include <stdlib.h>

#define POSIX_CHECK(stmt)									\
	do														\
	{														\
		if (!(stmt))										\
		{													\
			char msg[2048];									\
			snprintf(msg, sizeof(msg), "%s: %s:%d %s",		\
					 argv[0], __FILE__, __LINE__,			\
					 #stmt);								\
															\
			perror(msg);									\
															\
			return EXIT_FAILURE;							\
		}													\
	}														\
	while(0)

#include <math.h>
#include <string.h>

#include <unistd.h>

#include <mpi.h>

#define MPI_CHECK(stmt)								\
	do												\
	{												\
		const int err = stmt;						\
													\
		if (err != MPI_SUCCESS)						\
		{											\
			char msg[2048];							\
			int len = sizeof(msg);					\
			MPI_Error_string(err, msg, &len);		\
													\
			fprintf(stderr,							\
					"%s: %s:%d: %s %s\n",			\
					argv[0], __FILE__, __LINE__,	\
					#stmt, msg);					\
													\
			MPI_Abort(MPI_COMM_WORLD, err);			\
		}											\
	}												\
	while(0)

#include <fftw3-mpi.h>

#ifdef _FP32_
typedef float real;
typedef fftwf_plan plan;
typedef fftwf_complex complex;
#define FFTW_MPI_MANGLE FFTW_MPI_MANGLE_FLOAT
#else
typedef double real;
typedef fftw_plan plan;
typedef fftw_complex complex;
#define FFTW_MPI_MANGLE FFTW_MPI_MANGLE_DOUBLE
#endif

#define CHECK(stmt, ...)						\
	do											\
	{											\
		if (!(stmt))							\
		{										\
			fprintf(stderr,						\
					__VA_ARGS__);				\
												\
			exit(EXIT_FAILURE);					\
		}										\
	}											\
	while(0)


#ifndef MPI_CHECK
#endif

#define READENV(x, op)							\
	do											\
	{											\
		if (getenv(#x))							\
			x = op(getenv(#x));					\
	}											\
	while(0)

#define PRINTENV(f, x, p)						\
	fprintf(f, #x "=" p "\n", x)

int main(
	int argc,
	char ** argv)
{
	int ESTIMATE = 0;
	READENV(ESTIMATE, atoi);

	int VERBOSE = 0;
	READENV(VERBOSE, atoi);

	int NTIMES = 1;
	READENV(NTIMES, atoi);

	MPI_CHECK(MPI_Init(&argc, &argv));

	MPI_Comm comm = MPI_COMM_WORLD;

	int r, rn;
	MPI_CHECK(MPI_Comm_rank(comm, &r));
	MPI_CHECK(MPI_Comm_size(comm, &rn));

	if (argc != 4)
	{
		if (!r)
			fprintf(stderr,
					"usage: %s <xsize> <ysize> <zsize>\n",
					argv[0]);

		MPI_CHECK(MPI_Finalize());

		return !r ? EXIT_FAILURE : EXIT_SUCCESS;
	}
	else
		if (!r)
		{
			printf("sizeof(real): %d\n"
					"proceeding with %d MPI tasks\n",
					sizeof(real), rn);

			fprintf(stdout, "ENV VARS:\n");

			PRINTENV(stdout, ESTIMATE, "%d");
			PRINTENV(stdout, VERBOSE, "%d");
			PRINTENV(stdout, NTIMES, "%d");
		}

	const ptrdiff_t xn = atoll(argv[1]);
	const ptrdiff_t yn = atoll(argv[2]);
	const ptrdiff_t zn = atoll(argv[3]);

	const ptrdiff_t yxn = yn * xn;
	const ptrdiff_t zyxn = zn * yxn;

	/* SETUP */
	FFTW_MPI_MANGLE(init)();

	ptrdiff_t lzn = -1, lz0 = -1;
	ptrdiff_t mylzn = -1, mylz0 = -1, myln_xy = -1, myl0_xy = -1, myln_sca = -1;
	ptrdiff_t myzs = -1, myze = -1, lzn_max;

	{
		myln_sca = FFTW_MPI_MANGLE(local_size_2d_transposed)
			(zn, yxn, comm,
			 &mylzn, &mylz0, &myln_xy, &myl0_xy);

		if (mylzn == 0)
			mylz0 = zn;

		if (VERBOSE)
			printf("rank %d of %d will take z = [%d, %d) (%d) ln=%zd scalars (per quantity), ln_xy %d, l0_xy %d\n",
					r, rn,
					(int)mylz0, (int)(mylz0 + mylzn), (int)mylzn, myln_sca, (int)myln_xy, (int)myl0_xy);
	}

	MPI_CHECK(MPI_Barrier(comm));

	if (!r)
		fprintf(stderr, "allocating FFTW buffer\n");

	const size_t tmpfp = myln_sca * sizeof(real);

	real * tmpbuf = NULL;

	POSIX_CHECK(0 == posix_memalign
				((void **)&tmpbuf, sysconf(_SC_PAGESIZE), tmpfp));

	memset(tmpbuf, 0, myln_sca);

	if (!r)
		fprintf(stderr, "FFTW buffer allocated.\n");

	MPI_CHECK(MPI_Barrier(comm));

	unsigned estpat = ESTIMATE ? (-1 == ESTIMATE ? FFTW_EXHAUSTIVE : FFTW_ESTIMATE) : FFTW_PATIENT;

	plan fwdplan_sca = 0, bwdplan_sca = 0;

	fwdplan_sca = FFTW_MPI_MANGLE(plan_many_transpose)(
		zn, yxn, 1, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
		tmpbuf, tmpbuf, comm, estpat | FFTW_MPI_TRANSPOSED_OUT);

	bwdplan_sca = FFTW_MPI_MANGLE(plan_many_transpose)(
		yxn, zn, 1, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
		tmpbuf, tmpbuf, comm, estpat | FFTW_MPI_TRANSPOSED_OUT);

	CHECK(fwdplan_sca && bwdplan_sca,
		  "error (rank %d) could not create plans\n", r);

	MPI_CHECK(MPI_Barrier(comm));

	if (fwdplan_sca && bwdplan_sca && !r)
		fprintf(stderr, "plans created successfully.\n");

	MPI_CHECK(MPI_Barrier(comm));

#ifdef __clang__
	void (^nancheck)(const char *) = ^
#else
	__extension__ void nancheck
#endif
	(
		const char * const title)
	{
		int snan = 0;
		for(ptrdiff_t i = 0; i < myln_sca; ++i)
			snan |= isnan(tmpbuf[i]);

		if (snan)
		{
			printf("error (rank %d): some entries are NaN\n", r);

			exit(EXIT_FAILURE);
		}

		if (!r)
			printf("NANCHECK %s passed.\n", title);
	};

	if (!r)
		fprintf(stderr, "warmup FFTW call...\n");

	nancheck("IC");

	/* WARMUP CALL */
	FFTW_MPI_MANGLE(execute_r2r)(fwdplan_sca, tmpbuf, tmpbuf);

	nancheck("FWD");

	FFTW_MPI_MANGLE(execute_r2r)(bwdplan_sca, tmpbuf, tmpbuf);

	nancheck("BWD");

	if (!r)
		fprintf(stderr, "warmup FFTW call done.\n");

	/* BENCHMARK STARTS NOW */

	const double tstart = MPI_Wtime();

	for (int t = 0; t < NTIMES; ++t)
		FFTW_MPI_MANGLE(execute_r2r)(fwdplan_sca, tmpbuf, tmpbuf);

	const double tmiddle = MPI_Wtime();

	for (int t = 0; t < NTIMES; ++t)
		FFTW_MPI_MANGLE(execute_r2r)(bwdplan_sca, tmpbuf, tmpbuf);

	const double tend = MPI_Wtime();
#ifdef __clang__
	double (^tts) (double, double) = ^
#else
	__extension__ double tts
#endif
	(
		double t1,
		double t0 )
	{
		MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &t0, 1, MPI_DOUBLE, MPI_MIN, comm));
		MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &t1, 1, MPI_DOUBLE, MPI_MAX, comm));

		return (t1 - t0) / NTIMES;
	};

	double fpgb = zyxn * sizeof(real) * 2 * 1e-9;

	const double tts_a = tts(tmiddle, tstart);
	const double tts_b = tts(tend, tmiddle);
	const double tts_c = tts(tend, tstart);

	if (!r)
		fprintf(stderr,
				"forward: %.3fs, %.3fGB/s\n"
				"backward: %.3fs, %.3fGB/s\n"
				"forward + backward A2A took %.3f seconds. bye.\n",
				tts_a, fpgb / tts_a,
				tts_b, fpgb / tts_b,
				tts_c);

	free(tmpbuf);

	FFTW_MPI_MANGLE(cleanup)();

	MPI_CHECK(MPI_Finalize());

	return EXIT_SUCCESS;
}
