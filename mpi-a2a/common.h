#include <stdlib.h>
#include <stdio.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define POSIX_CHECK(stmt)									\
	do														\
	{														\
		if (!(stmt))										\
		{													\
			char msg[2048];									\
			snprintf(msg, sizeof(msg), "%s:%d: %s\n",		\
					 __FILE__, __LINE__, #stmt);			\
															\
			perror(msg);									\
															\
			exit(EXIT_FAILURE);								\
		}													\
	}														\
	while(0)

#define MPI_CHECK(stmt)								\
	do												\
	{												\
		const int err = stmt;						\
													\
		if (err != MPI_SUCCESS)						\
		{											\
			char msg[2048];							\
			int len = sizeof(msg);					\
			MPI_Error_string(err, msg, &len);		\
													\
			fprintf(stderr,							\
					"%s:%d: %s %s\n",				\
					__FILE__, __LINE__,				\
					#stmt, msg);					\
													\
			MPI_Abort(MPI_COMM_WORLD, err);			\
		}											\
	}												\
	while(0)

#define CHECK(stmt, ...)						\
	do											\
	{											\
		if (!(stmt))							\
		{										\
			fprintf(stderr,						\
					__VA_ARGS__);				\
												\
			exit(EXIT_FAILURE);					\
		}										\
	}											\
	while(0)


#ifndef MPI_CHECK
#endif

#define READENV(x, op)							\
	do											\
	{											\
		if (getenv(#x))							\
			x = op(getenv(#x));					\
	}											\
	while(0)

#define PRINTENV(f, x, p)						\
	fprintf(f, #x "=" p "\n", x)
