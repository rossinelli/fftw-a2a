#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <mpi.h>

#include "transpose.h"
#include "common.h"

#ifdef _I32_
typedef uint32_t payload_t;
#else
typedef uint64_t payload_t;
#endif

int main (
	int argc,
	char ** argv)
{
	int VERBOSE = 0;
	READENV(VERBOSE, atoi);

	int NTIMES = 1;
	READENV(NTIMES, atoi);

	MPI_CHECK(MPI_Init(&argc, &argv));

	MPI_Comm comm = MPI_COMM_WORLD;

	int r, rn;
	MPI_CHECK(MPI_Comm_rank(comm, &r));
	MPI_CHECK(MPI_Comm_size(comm, &rn));

	if (argc != 3)
	{
		if (r == 0)
			fprintf(stderr,
					"usage: %s <xsize> <ysize>\n",
					argv[0]);

		return r ? EXIT_SUCCESS : EXIT_FAILURE;
	}

	if (!r)
	{
		printf("sizeof(real): %d\n"
			   "proceeding with %d MPI tasks\n",
			   (int)sizeof(int32_t), rn);

		fprintf(stdout, "ENV VARS:\n");

		PRINTENV(stdout, NTIMES, "%d");
		PRINTENV(stdout, VERBOSE, "%d");
	}

	const ptrdiff_t xn = atoi(argv[1]);
	const ptrdiff_t yn = atoi(argv[2]);

	transpose_t tinfo = transpose_init(comm, yn, xn);

	CHECK(xn == tinfo.xn && yn == tinfo.yn,
		  "error: size mismatch\n");

	const ptrdiff_t __attribute__((unused)) xnpr = tinfo.xnpr;
	const ptrdiff_t __attribute__((unused)) ynpr = tinfo.ynpr;
	const ptrdiff_t __attribute__((unused)) yxnpr = tinfo.yxnpr;
	const ptrdiff_t __attribute__((unused)) x0 = tinfo.x0;
	const ptrdiff_t __attribute__((unused)) y0 = tinfo.y0;
	const ptrdiff_t __attribute__((unused)) xnpad = tinfo.xnpad;
	const ptrdiff_t __attribute__((unused)) ynpad = tinfo.ynpad;

	if (VERBOSE)
		fprintf(stderr,
				"hi from rank %d of %d\n", r, rn);

	if (!r)
		fprintf(stderr, "padded image: %d x %d, tile: %d x %d \n",
				(int)xnpad, (int)ynpad, (int)xnpr, (int)ynpr);

	payload_t * data = malloc(sizeof(payload_t) * rn * yxnpr);
	payload_t * tmp = malloc(sizeof(payload_t) * rn * yxnpr);

	for (ptrdiff_t y = 0; y < MIN(ynpr, yn - y0); ++y)
		for (ptrdiff_t x = 0; x < xn; ++x)
			tmp[x + xnpad * y] = (payload_t)(x + xn * (y + y0));

	transpose(tinfo, sizeof(payload_t), tmp, data);

	for (ptrdiff_t x = 0; x < MIN(xnpr, xn - x0); ++x)
		for(ptrdiff_t y = 0; y < yn; ++y)
		{
			const ptrdiff_t entry = y + ynpad * x;
			const payload_t expected = (payload_t)(x0 + x + xn * y);

			CHECK(data[entry] == expected,
				  "oops rank %d not as expected: data[%d] = %d and not %d\n",
				  r, entry, data[entry], expected);
		}

	MPI_CHECK(MPI_Barrier(comm));

	const double t0 = MPI_Wtime();

	for(int t = 0; t < NTIMES; ++t)
		transpose(tinfo, sizeof(payload_t), tmp, data);

	MPI_CHECK(MPI_Barrier(comm));

	const double tavg = (MPI_Wtime() - t0) / NTIMES;

	if (!r)
	{
		fprintf(stderr, "average transpose time: %.3fms\n", 1e3 * tavg);
		fprintf(stderr, "BW: %.3f GB/s\n", xn * yn * sizeof(payload_t) * 2e-9 / tavg);
	}

	free(tmp);
	free(data);

	MPI_CHECK(MPI_Finalize());

	return EXIT_SUCCESS;
}
