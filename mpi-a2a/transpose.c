#include <stddef.h>

#include <assert.h>
#include <string.h>

#include <mpi.h>

#include "transpose.h"
#include "common.h"

#define _XPAND_ARGS_()											\
	MPI_Comm comm = w.comm;										\
																\
	int r, rn;													\
	MPI_CHECK(MPI_Comm_rank(comm, &r));							\
	MPI_CHECK(MPI_Comm_size(comm, &rn));						\
																\
	const ptrdiff_t __attribute__((unused)) xn = w.xn;			\
	const ptrdiff_t __attribute__((unused)) yn = w.yn;			\
	const ptrdiff_t __attribute__((unused)) xnpr = w.xnpr;		\
	const ptrdiff_t __attribute__((unused)) ynpr = w.ynpr;		\
	const ptrdiff_t __attribute__((unused)) yxnpr = w.yxnpr;	\
	const ptrdiff_t __attribute__((unused)) x0 = w.x0;			\
	const ptrdiff_t __attribute__((unused)) y0 = w.y0;			\
	const ptrdiff_t __attribute__((unused)) xnpad = w.xnpad;	\
	const ptrdiff_t __attribute__((unused)) ynpad = w.ynpad;	\
	const ptrdiff_t __attribute__((unused)) shm = w.shm;

static int TRANSPOSE_VERBOSE = 0, TRANSPOSE_FINEGRAIN = 0, TRANSPOSE_A2ASZ = 0, TRANSPOSE_BAR = 0;

transpose_t transpose_init (
	MPI_Comm comm,
	const ptrdiff_t yn,
	const ptrdiff_t xn)
{
	transpose_t retval;

	retval.comm = comm;

	int r, rn;
	MPI_CHECK(MPI_Comm_rank(comm, &r));
	MPI_CHECK(MPI_Comm_size(comm, &rn));

	retval.xn = xn;
	retval.yn = yn;
	retval.xnpr = (xn + rn - 1) / rn;
	retval.ynpr = (yn + rn - 1) / rn;
	retval.yxnpr = retval.ynpr * retval.xnpr;
	retval.x0 = retval.xnpr * r;
	retval.y0 = retval.ynpr * r;
	retval.xn = xn;
	retval.yn = yn;

	retval.xnpad = retval.xnpr * rn;
	retval.ynpad = retval.ynpr * rn;
	retval.eln = retval.xnpr * retval.ynpr * rn;

	assert(retval.ynpr * rn == retval.ynpad);
	assert(retval.xnpr * rn == retval.xnpad);

	return retval;
}

ptrdiff_t generic_elsz = 0;

void generic_tra (
	void * const __restrict__ in,
	const int yn,
	const int xn,
	void * const __restrict__ out )
{
	for (ptrdiff_t x = 0; x < xn; ++x)
		for (ptrdiff_t y = 0; y < yn; ++y)
			memcpy(generic_elsz * (y + yn * x) + (char *)out,
				   generic_elsz * (x + xn * y) + (char *)in,
				   generic_elsz);
}

void generic_cpy (
	void * const __restrict__ out,
	const void * const __restrict__ in,
	const ptrdiff_t n,
	const ptrdiff_t stride)
{
	for (ptrdiff_t i = 0; i < n; ++i)
		memcpy(generic_elsz * i + (char *)out,
			   generic_elsz * i * stride + (char *)in,
			   generic_elsz);
}

#define DEFF(myname, mytype)						\
	static void myname (							\
		void * const __restrict__ _out,				\
		const void * const __restrict__ _in,		\
		const ptrdiff_t n,							\
		const ptrdiff_t stride)						\
	{												\
		mytype * const __restrict__ out = _out;		\
		const mytype * const __restrict__ in = _in;	\
													\
		for(ptrdiff_t i = 0; i < n; ++i)			\
			out[i] = in[stride * i];				\
	}

DEFF(memcpy_strided_i8, uint8_t)
DEFF(memcpy_strided_i16, uint16_t)
DEFF(memcpy_strided_i32, uint32_t)
DEFF(memcpy_strided_i64, uint64_t)

static void a2a (
	const void * in,
	/* byte count of a single message */
	const ptrdiff_t bc,
	void * out,
	MPI_Comm comm )
{
	if (!TRANSPOSE_A2ASZ)
	{
		MPI_CHECK(MPI_Alltoall
				  (in, bc, MPI_BYTE,
				   out, bc, MPI_BYTE, comm));

		return;
	}

	int rc = 0;
	MPI_CHECK(MPI_Comm_size(comm, &rc));

	ptrdiff_t s = MIN(bc, TRANSPOSE_A2ASZ);

	void *sbuf = NULL, *rbuf = NULL;
	POSIX_CHECK(sbuf = malloc(s * rc));
	POSIX_CHECK(rbuf = malloc(s * rc));

	for (ptrdiff_t b = 0; b < bc; b += s)
	{
		/* actual size */
		const ptrdiff_t as = MIN(s, bc - b);

		for (ptrdiff_t r = 0; r < rc; ++r)
			memcpy(as * r + (uint8_t *)sbuf, b + bc * r + (uint8_t *)in, as);

		MPI_CHECK(MPI_Alltoall
				  (sbuf, as, MPI_BYTE,
				   rbuf, as, MPI_BYTE, comm));

		for (ptrdiff_t r = 0; r < rc; ++r)
			memcpy(b + bc * r + (uint8_t *)out, as * r + (uint8_t *)rbuf, as);
	}

	free(rbuf);
	free(sbuf);
}

void transpose (
	const transpose_t w,
	const ptrdiff_t elsz,
	void * const __restrict__ src,
	void * const __restrict__ dst )
{
	_XPAND_ARGS_();

	double t0 = MPI_Wtime();

	generic_elsz = elsz;

	__typeof__(generic_tra) * ktra = generic_tra;
	__typeof__(memcpy_strided_i32) * kcpy = NULL;

	if (8 == elsz)
		kcpy = memcpy_strided_i64;
	else if (4 == elsz)
		kcpy = memcpy_strided_i32;
	else if (2 == elsz)
		kcpy = memcpy_strided_i16;
	else if (1 == elsz)
		kcpy = memcpy_strided_i8;
	else
	{
		ktra = generic_tra;
		kcpy = generic_cpy;
	}

	if (TRANSPOSE_FINEGRAIN)
	{
		void * yline = malloc(elsz * ynpad);

		/* the idea is to overlap the MPI communication
		   with the bad access pattern of the non-locally transposed data */
		for (ptrdiff_t x = 0; x < xnpr; ++x)
		{
			for (ptrdiff_t r = 0; r < rn; ++r)
				kcpy((uint8_t *)yline + elsz * ynpr * r,
					 (uint8_t *)src + elsz * (x + xnpr * r),
					 ynpr, xnpad);

			a2a(yline, ynpr * elsz, elsz * (ynpr * rn * x) + (uint8_t*)dst, comm);
		}

		free(yline);
	}
	else
	{
		ktra(src, ynpr, xnpad, dst);

		a2a(dst, yxnpr * elsz, src, comm);

		/* KNL clusters needed a barrier here: */
		if (TRANSPOSE_BAR)
			MPI_CHECK(MPI_Barrier(comm));

		for (ptrdiff_t x = 0; x < xnpr; ++x)
			for (ptrdiff_t r = 0; r < rn; ++r)
				memcpy((uint8_t *)dst + elsz * ynpr * (r + rn * x),
					   (uint8_t *)src + elsz * ynpr * (x + xnpr * r),
					   elsz * ynpr);
	}

	if (TRANSPOSE_VERBOSE)
	{
		double t1 = MPI_Wtime();

		MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &t0, 1, MPI_DOUBLE, MPI_MIN, comm));
		MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &t1, 1, MPI_DOUBLE, MPI_MAX, comm));

		int r;
		MPI_CHECK(MPI_Comm_rank(comm, &r));

		if (!r)
		{
			PRINTENV(stderr, TRANSPOSE_VERBOSE, "%d");
			PRINTENV(stderr, TRANSPOSE_FINEGRAIN, "%d");
			PRINTENV(stderr, TRANSPOSE_A2ASZ, "%d");
			PRINTENV(stderr, TRANSPOSE_BAR, "%d");

			fprintf(stderr,
					"transpose_mpi: elsz: %zd finegrain %d, a2asize %d, barrier: %d, BW %.3f GB/s\n",
					elsz, TRANSPOSE_FINEGRAIN, TRANSPOSE_A2ASZ, TRANSPOSE_BAR, ynpad * xnpad * elsz * 2e-9 / (t1 - t0));
		}
	}

	if (!r)
	{
		static int printed = 0;

		if (!printed)
		{
			PRINTENV(stderr, TRANSPOSE_VERBOSE, "%d");
			PRINTENV(stderr, TRANSPOSE_FINEGRAIN, "%d");
			PRINTENV(stderr, TRANSPOSE_A2ASZ, "%d");
			PRINTENV(stderr, TRANSPOSE_BAR, "%d");
		}

		printed = 1;
	}
}

void __attribute__((constructor)) init()
{
	READENV(TRANSPOSE_VERBOSE, atoi);
	READENV(TRANSPOSE_FINEGRAIN, atoi);
	READENV(TRANSPOSE_A2ASZ, atoi);
	READENV(TRANSPOSE_BAR, atoi);
}
