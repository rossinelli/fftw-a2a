#pragma once

#include <stdio.h>
#include <string.h>

#include <mpi.h>

#define MPI_PTRDIFF_T MPI_LONG_LONG

#ifdef _FP32_
static const MPI_Datatype rtype = MPI_FLOAT;
#else
static const MPI_Datatype rtype = MPI_DOUBLE;
#endif

#ifndef MPI_CHECK
#define MPI_CHECK(stmt)                                         \
    do                                                          \
    {                                                           \
	const int code = stmt;                                  \
                                                                \
	if (code != MPI_SUCCESS)                                \
	{                                                       \
	    char error_string[2048];                            \
	    int length_of_error_string = sizeof(error_string);  \
	    MPI_Error_string(code, error_string,                \
			     &length_of_error_string);          \
                                                                \
	    fprintf(stderr,                                     \
		    "ERROR!\n" #stmt " mpiAssert: %s %d %s\n",  \
		    __FILE__, __LINE__, error_string);          \
	    fflush(stderr);                                     \
                                                                \
	    MPI_Abort(MPI_COMM_WORLD, code);                    \
	}                                                       \
    }                                                           \
    while(0)
#endif

#include <math.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

inline static
int count_slices (
    MPI_File f,
    size_t nbytes_slice)
{
    MPI_Offset fsize;
    MPI_CHECK(MPI_File_get_size(f, &fsize));

    return fsize / nbytes_slice;
}

inline static
int count_slices_pathname (
    const char * const pathname,
    const size_t nbytes_slice)
{
    int rank;
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

    int retval = -1;

    if (rank == 0)
    {
        MPI_File fin;

        MPI_CHECK(MPI_File_open(MPI_COMM_SELF, (char *)pathname,
                                MPI_MODE_RDONLY, MPI_INFO_NULL, &fin));

        retval = count_slices(fin, nbytes_slice);

        MPI_CHECK(MPI_File_close(&fin));
    }

    MPI_CHECK(MPI_Bcast(&retval, 1, MPI_INT, 0, MPI_COMM_WORLD));

    return retval;
}
