#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>
#include <mpi-util.h>

#include <nt.h>

#ifndef MAX
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif

#ifndef MIN
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif

#define CHECK(stmt, ...)			\
    do						\
    {						\
	if (!(stmt))				\
	{					\
	    fprintf(stderr,			\
		    __VA_ARGS__);		\
						\
	    exit(EXIT_FAILURE);			\
	}					\
    }						\
    while(0)

#define READENV(x, op)				\
    do						\
    {						\
	if (getenv(#x))				\
	    x = op(getenv(#x));			\
    }						\
    while(0)

#define PRINTENV(f, x, p)			\
    fprintf(f, #x ": " p "\n", x)

int main (
    int argc,
    char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    nt_init();
    nt_batch();

    int VERBOSE = 0;
    READENV(VERBOSE, atoi);

    int NTIMES = 1;
    READENV(NTIMES, atoi);

    int FINEGRAIN = 0;
    READENV(FINEGRAIN, atoi);

    int PERNUMA = 0;
    READENV(PERNUMA, atoi);

    int PERNODE = 0;
    READENV(PERNODE, atoi);

    int NOCOLL = 0;
    READENV(NOCOLL, atoi);

    MPI_Comm comm = MPI_COMM_WORLD;

    if (PERNUMA)
    {
	comm = nt_core.comm;
	PERNODE = 0;
    }
    else if (PERNODE)
    {
	comm = nt_numa.comm;
	PERNUMA = 0;
    }

    int r, rn;
    MPI_CHECK(MPI_Comm_rank(comm, &r));
    MPI_CHECK(MPI_Comm_size(comm, &rn));

    int g, gn;
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &g));
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &gn));

    if (argc != 3)
    {
	if (r == 0)
	    fprintf(stderr,
		    "usage: %s <xsize> <ysize>\n",
		    argv[0]);

	return r ? EXIT_SUCCESS : EXIT_FAILURE;
    }

    if (!g)
    {
	printf("sizeof(real): %d\n"
	       "proceeding with %d MPI tasks\n",
	       sizeof(int32_t), rn);

	fprintf(stdout, "ENV VARS:\n");

	PRINTENV(stdout, NTIMES, "%d");
	PRINTENV(stdout, PERNUMA, "%d");
	PRINTENV(stdout, PERNODE, "%d");
	PRINTENV(stdout, NOCOLL, "%d");
	PRINTENV(stdout, VERBOSE, "%d");
	PRINTENV(stdout, FINEGRAIN, "%d");
    }

    const ptrdiff_t xn = atoi(argv[1]);
    const ptrdiff_t yn = atoi(argv[2]);

    if (VERBOSE)
	fprintf(stderr,
		"hi from rank %d of %d\n", r, rn);

    const ptrdiff_t xnpr = (xn + rn - 1) / rn;
    const ptrdiff_t ynpr = (yn + rn - 1) / rn;
    const ptrdiff_t yxnpr = ynpr * xnpr;

    const ptrdiff_t x0 = xnpr * r;
    const ptrdiff_t y0 = ynpr * r;

    const ptrdiff_t xnpad = xnpr * rn;
    const ptrdiff_t ynpad = ynpr * rn;

    if (!g)
	fprintf(stderr, "padded image: %d x %d, tile: %d x %d \n",
		xnpad, ynpad, xnpr, ynpr);

    int32_t * data = malloc(sizeof(int32_t) * rn * yxnpr);
    int32_t * tmp = malloc(sizeof(int32_t) * rn * yxnpr);

    int32_t * yline = NULL;

    if (FINEGRAIN == 1)
	yline = malloc(sizeof(int32_t) * ynpad);

    if (FINEGRAIN == 2)
	for (int y = 0; y < MIN(ynpr, yn - y0); ++y)
	    for (int x = 0; x < xn; ++x)
	    {
		const ptrdiff_t xt = x % xnpr;
		const ptrdiff_t r = x / xnpr;
		const ptrdiff_t entry = y + ynpr * (r + rn * xt);
	    	data[entry] = x + xn * (y + y0);
	    }
    else
	for (int x = 0; x < xn; ++x)
	    for (int y = 0; y < MIN(ynpr, yn - y0); ++y)
		data[y + ynpr * x] = x + xn * (y + y0);

    if (FINEGRAIN)
	memcpy(tmp, data, sizeof(int32_t) * rn * yxnpr);

    int32_t * shbuf = NULL;

    if ((PERNUMA || PERNODE) && NOCOLL)
    {
	shbuf = nt_shmem(PERNUMA ? nt_core : nt_numa, 
			 sizeof(int32_t) * rn * yxnpr * rn);

	memcpy(shbuf + yxnpr * rn * r, data,
	       sizeof(int32_t) * rn * yxnpr);
    }

    __extension__ void transpose()
    {
	if ((PERNUMA || PERNODE) && NOCOLL)
	{
	    MPI_CHECK(MPI_Barrier(nt_core.comm));

	    if (FINEGRAIN == 2)
		for (ptrdiff_t x = 0; x < xnpr; ++x)
		    for (ptrdiff_t rr = 0; rr < rn; ++rr)
			memcpy(data + ynpr * (rr + rn * x),
			       shbuf + ynpr * (r + rn * (x + xnpr * rr)),
			       sizeof(int32_t) * ynpr);
	    else
		for (ptrdiff_t x = 0; x < xnpr; ++x)
		    for (ptrdiff_t rr = 0; rr < rn; ++rr)
			memcpy(data + ynpr * (rr + rn * x),
			       shbuf + ynpr * (x + xnpr * (r + rn * rr)),
			       sizeof(int32_t) * ynpr);

	    MPI_CHECK(MPI_Barrier(nt_core.comm));
	}
	else if (FINEGRAIN == 2)
	    for (ptrdiff_t x = 0; x < xnpr; ++x)
	    	MPI_CHECK(MPI_Alltoall
			  (tmp + ynpad * x, ynpr, MPI_INT,
			   data + ynpad * x, ynpr, MPI_INT, comm));
	else if (FINEGRAIN == 1)
	    for (ptrdiff_t x = 0; x < xnpr; ++x)
	    {
		for (ptrdiff_t r = 0; r < rn; ++r)
		    memcpy(yline + ynpr * r,
			   tmp + ynpr * (x + xnpr * r),
			   sizeof(int32_t) * ynpr);

		MPI_CHECK(MPI_Alltoall
			  (yline , ynpr, MPI_INT,
			  data+ ynpr * rn * x, ynpr, MPI_INT, comm));
	    }
	else
	{
	    MPI_CHECK(MPI_Alltoall
		      (data, yxnpr, MPI_INT, tmp, yxnpr, MPI_INT, comm));

	    for (ptrdiff_t x = 0; x < xnpr; ++x)
		for (ptrdiff_t r = 0; r < rn; ++r)
		    memcpy(data + ynpr * (r + rn * x),
			   tmp + ynpr * (x + xnpr * r), sizeof(int32_t) * ynpr);
	}
    }

    transpose();

    for (int x = 0; x < MIN(xnpr, xn - x0); ++x)
	for(int y = 0; y < yn; ++y)
	{
	    const int32_t entry = y + ynpad * x;
	    const int32_t expected = x0 + x + xn * y;

	    CHECK(data[entry] == expected,
		  "oops rank %d not as expected: data[%d] = %d and not %d\n",
		  r, entry, data[entry], expected);
	}

    MPI_CHECK(MPI_Barrier(comm));

    double t0 = MPI_Wtime();

    for(int t = 0; t < NTIMES; ++t)
	transpose();

    MPI_CHECK(MPI_Barrier(comm));

    double t1 = MPI_Wtime();

    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &t0, 1, MPI_DOUBLE, MPI_MIN, comm));
    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &t1, 1, MPI_DOUBLE, MPI_MAX, comm));

    double tavg = (t1 - t0) / NTIMES;

    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &tavg, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD));

    tavg /= gn;

    if (!g)
    {
	fprintf(stderr, "average transpose time: %.3fms\n",
		1e3 * tavg);

	if (PERNUMA)
	    fprintf(stderr, "BW: %.3f GB/s (aggregate over a set of %d problems)\n",
		    nt_numa.global_count * xn * yn * sizeof(int32_t) * 2e-9 / tavg,
		    nt_numa.global_count);
	else if (PERNODE)
	    fprintf(stderr, "BW: %.3f GB/s (aggregate over a set of %d problems)\n",
		    nt_node.global_count * xn * yn * sizeof(int32_t) * 2e-9 / tavg,
		    nt_node.global_count);
	else
	    fprintf(stderr, "BW: %.3f GB/s (1 global problem)\n",
		    xn * yn * sizeof(int32_t) * 2e-9 / tavg);

    }

    if (yline)
	free(yline);

    if (shbuf)
	nt_free(shbuf);

    free(tmp);
    free(data);

    nt_finalize();

    MPI_CHECK(MPI_Finalize());

    return EXIT_SUCCESS;
}
